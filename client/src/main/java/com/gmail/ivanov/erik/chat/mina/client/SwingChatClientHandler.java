package com.gmail.ivanov.erik.chat.mina.client;

import com.gmail.ivanov.erik.chat.mina.common.ChatCommand;
import com.gmail.ivanov.erik.chat.mina.common.ChatMessage;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;


public class SwingChatClientHandler extends IoHandlerAdapter {
    public interface Callback {
        void connected();

        void loggedIn(int id);

        void loggedOut();

        void disconnected();

        void messageReceived(String message);

        void error(String message);
    }

    private final Callback callback;

    public SwingChatClientHandler(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        callback.connected();
    }

    @Override
    public void messageReceived(IoSession session, Object message)
            throws Exception {
        ChatMessage chatMessage = (ChatMessage) message;
        if (chatMessage.getID() < 1 ) {
            callback.error(chatMessage.getValue());
            return;
        }
        ChatCommand command = ChatCommand.valueOf(chatMessage.getType());

        switch (command.toInt()) {
            case ChatCommand.BROADCAST:
                callback.messageReceived(chatMessage.getValue());
                break;
            case ChatCommand.LOGIN:
                int id = chatMessage.getID();
                callback.loggedIn(id);
                break;
            case ChatCommand.QUIT:
                callback.loggedOut();
                break;
            default:
                callback.error(chatMessage.toString());
        }
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        callback.disconnected();
    }

}
