package com.gmail.ivanov.erik.chat.mina.client;

import com.gmail.ivanov.erik.chat.mina.common.ChatCommand;
import com.gmail.ivanov.erik.chat.mina.common.ChatMessage;
import com.gmail.ivanov.erik.chat.mina.common.ssl.BogusSslContextFactory;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.filter.logging.MdcInjectionFilter;
import org.apache.mina.filter.ssl.SslFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import javax.net.ssl.SSLContext;
import java.net.SocketAddress;

/**
 * A simple chat client for a given user.
 */
public class ChatClientSupport {
    private final IoHandler handler;
    private final String name;
    private IoSession session;
    public  int id;

    public ChatClientSupport(String name, IoHandler handler) {
        if (name == null) {
            throw new IllegalArgumentException("Name can not be null");
        }
        this.name = name;
        this.handler = handler;
    }

    public boolean connect(NioSocketConnector connector, SocketAddress address,
                           boolean useSsl) {
        if (session != null && session.isConnected()) {
            throw new IllegalStateException(
                    "Already connected. Disconnect first.");
        }
        try {
            connector.getFilterChain().clear();
            connector.getFilterChain().addLast("mdc", new MdcInjectionFilter());
            connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));
            connector.getFilterChain().addLast("logger", new LoggingFilter());
            if (useSsl) {
                SSLContext sslContext = BogusSslContextFactory
                        .getInstance(false);
                SslFilter sslFilter = new SslFilter(sslContext);
                sslFilter.setUseClientMode(true);
                connector.getFilterChain().addFirst("sslFilter", sslFilter);
            }
            connector.setHandler(handler);
            ConnectFuture future1 = connector.connect(address);
            future1.awaitUninterruptibly();
            if (!future1.isConnected()) {
                return false;
            }
            session = future1.getSession();
            login();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void login() {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setType(ChatCommand.LOGIN);
        chatMessage.setValue(name);
        session.write(chatMessage);
    }

    public void broadcast(String message) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setID(id);
        chatMessage.setType(ChatCommand.BROADCAST);
        chatMessage.setValue(message);
        session.write(chatMessage);
    }

    public void quit() {
        if (session != null) {
            if (session.isConnected()) {
                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setType(ChatCommand.QUIT);
                chatMessage.setID(id);
                session.write(chatMessage);
                // Wait until the chat ends.
                session.getCloseFuture().awaitUninterruptibly();
            }
            session.close(true);
        }
    }
}