package com.gmail.ivanov.erik.chat.mina.sever;

import com.gmail.ivanov.erik.chat.mina.common.*;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.logging.MdcInjectionFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * {@link IoHandler} implementation of a simple chat server protocol.
 */

public class ChatProtocolHandler extends IoHandlerAdapter {
    private final static Logger LOGGER = LoggerFactory.getLogger(ChatProtocolHandler.class);
    private final Set<IoSession> sessions = Collections.synchronizedSet(new HashSet<IoSession>());
//    private final Set<String> users = Collections.synchronizedSet(new HashSet<String>());
    private final List<String> users = Collections.synchronizedList(new ArrayList<String>());

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) {
        LOGGER.warn("Unexpected exception.", cause);
        // Close connection when unexpected exception is caught.
        session.close(true);
    }

    @Override
    public void messageReceived(IoSession session, Object message) {
        Logger log = LoggerFactory.getLogger(ChatProtocolHandler.class);
        ChatMessage chatMessage = (ChatMessage) message;

        log.info("received: " + chatMessage.toString());
        try {
            ChatCommand command = ChatCommand.valueOf(chatMessage.getType());
            String user = null;
            int id;
            try {
                id = (int) session.getAttribute("id");
            } catch (Exception e ) {
                id = 0;
            }

            if (id > 0) {
                user =  users.get(id - 1);
            }

            switch (command.toInt()) {
                case ChatCommand.QUIT:
                    chatMessage.setValue("QUIT OK");
                    chatMessage.setID(id);
                    session.write(chatMessage);
                    session.close(true);
                    break;
                case ChatCommand.LOGIN:
                    if (user != null) {
                        chatMessage.setValue("LOGIN ERROR user " + user  + " already logged in.");
                        chatMessage.setID(id);
                        session.write(chatMessage);
                        return;
                    }
                    if (!chatMessage.getValue().isEmpty()) {
                        user = chatMessage.getValue();
                    } else {
                        chatMessage.setValue("LOGIN ERROR invalid login command.");
                        chatMessage.setID(id);
                        session.write(chatMessage);
                        return;
                    }
                    // check if the username is already used
                    if (users.contains(user)) {
                        chatMessage.setID(id);
                        chatMessage.setValue("LOGIN ERROR the name " + user + " is already used.");
                        session.write(chatMessage);
                        return;
                    }
                    // Allow all users
                    users.add(id, user);
                    id =+ 1;
                    sessions.add(session);
                    session.setAttribute("id", id);
                    MdcInjectionFilter.setProperty(session, "user", user);

                    chatMessage.setID(id);
                    session.write(chatMessage);
                    broadcast("The user " + user + " has joined the chat session.");
                    break;
                case ChatCommand.BROADCAST:
                    broadcast(user + ": " + chatMessage.getValue());
                    break;
                default:
                    LOGGER.info("Unhandled command: " + command);
                    break;
            }
        } catch (IllegalArgumentException e) {
            LOGGER.debug("Illegal argument", e);
        }
    }

    public void broadcast(String message) {
        synchronized (sessions) {
            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setValue(message);
            chatMessage.setID(Integer.MAX_VALUE);
            chatMessage.setType(ChatCommand.BROADCAST);
            for (IoSession session : sessions) {
                if (session.isConnected()) {
                    session.write(chatMessage);
                }
            }
        }
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        int id;
        String user = null;
        try {
            id = (int) session.getAttribute("id");
        } catch (Exception e) {
            id = 0;
        }

        if (id > 0) {
            user = users.get(id - 1);
            users.remove(id-1);
        }

        sessions.remove(session);
        broadcast("The user " + user + " has left the chat session.");
    }
    @Override
    public void sessionCreated(IoSession session) throws Exception {
        String user = (String) session.getAttribute("user");
        LOGGER.debug("create: " + user);
    }

    public boolean isChatUser(String name) {
        return users.contains(name);
    }

    public int getNumberOfUsers() {
        return users.size();
    }

    public void kick(String name) {
        synchronized (sessions) {
            for (IoSession session : sessions) {
                if (name.equals(session.getAttribute("user"))) {
                    session.close(true);
                    break;
                }
            }
        }
    }

}
