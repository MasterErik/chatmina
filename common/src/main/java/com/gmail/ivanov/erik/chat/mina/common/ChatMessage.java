package com.gmail.ivanov.erik.chat.mina.common;

import java.util.IdentityHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 15.06.14
 * Time: 19:36
 * To change this template use File | Settings | File Templates.
 */
public class ChatMessage extends AbstractMessage {
    private static final long serialVersionUID = -940833727168119141L;
    private String value;
    private int ID;
    public ChatMessage() {
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Sequence=" + getSequence() +" Type(" + getType() +')' + " Msg(" + getValue() + ")";
    }

}
