package com.gmail.ivanov.erik.chat.mina.common;

public class ChatCommand {
    public static final int LOGIN = 0;
    public static final int QUIT = 1;
    public static final int BROADCAST = 2;
    private final int num;

    private ChatCommand(int num) {
        this.num = num;
    }

    public int toInt() {
        return num;
    }

    public static ChatCommand valueOf(int value) {
        switch (value) {
            case LOGIN:     return new ChatCommand(LOGIN);
            case QUIT:      return new ChatCommand(QUIT);
            case BROADCAST: return new ChatCommand(BROADCAST);
            default:
                throw new IllegalArgumentException("Unrecognized command: " + value);
        }

    }
}


