package com.gmail.ivanov.erik.chat.mina.common;

/**
 * Created with IntelliJ IDEA.
 * User: Erik
 * Date: 15.06.14
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

public abstract class AbstractMessage implements Serializable {
    private int sequence;
    private int type;
    public int getSequence() {
        return sequence;
    }
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
}
